#include "functions.h"
#include "hangman.h"
//Copyright Niklas Haiden, Fabian Hofstaetter, Alexander Temmel
void Hangman()
{
    char dummy;
    char schwkeit, eingabe;
    char word[] = "000000000000000", speicher[] = "_______________";
    int randzahl, trys = 0, round = 0;
    char eingegeben[1000];

    printf("Welche Schwierigkeit?\n(l) Leicht\n(m) Mittel\n(s) Schwierig\n(S) Spezial\n\t--> ");
    schwkeit = getch();

    switch(schwkeit)
    {
    case 'l':
        randzahl = Random(10,1);
        break;
    case 'm':
        randzahl = Random(10,2);
        break;
    case 's':
        randzahl = Random(10,3);
        break;
    case 'S':
        randzahl = Random(10,4);
        break;
    default:
        Start();
        break;
    }

    switch(randzahl)
    {
    case 0:
        word[0]='h';
        word[1]='a';
        word[2]='l';
        word[3]='l';
        word[4]='o';
        break;
    case 1:
        word[0]='g';
        word[1]='e';
        word[2]='r';
        word[3]='m';
        word[4]='a';
        word[5]='n';
        word[6]='y';
        break;
    case 2:
        word[0]='t';
        word[1]='i';
        word[2]='g';
        word[3]='e';
        word[4]='r';
        break;
    case 3:
        word[0]='f';
        word[1]='r';
        word[2]='a';
        word[3]='n';
        word[4]='k';
        word[5]='r';
        word[6]='e';
        word[7]='i';
        word[8]='c';
        word[9]='h';
        break;
    case 4:
        word[0]='b';
        word[1]='o';
        word[2]='m';
        word[3]='b';
        word[4]='e';
        word[5]='r';
        break;
    case 5:
        word[0]='s';
        word[1]='p';
        word[2]='a';
        word[3]='g';
        word[4]='h';
        word[5]='e';
        word[6]='t';
        word[7]='t';
        word[8]='i';
        break;
    case 6:
        word[0]='s';
        word[1]='c';
        word[2]='h';
        word[3]='o';
        word[4]='k';
        word[5]='i';
        break;
    case 7:
        word[0]='n';
        word[1]='e';
        word[2]='t';
        word[3]='f';
        word[4]='l';
        word[5]='i';
        word[6]='x';
        break;
    case 8:
        word[0]='b';
        word[1]='a';
        word[2]='u';
        word[3]='m';
        break;
    case 9:
        word[0]='a';
        word[1]='u';
        word[2]='s';
        word[3]='t';
        word[4]='r';
        word[5]='i';
        word[6]='a';
        break;
    case 10:
        word[0]='a';
        word[1]='t';
        word[2]='o';
        word[3]='m';
        word[4]='b';
        word[5]='o';
        word[6]='m';
        word[7]='b';
        word[8]='e';
        break;
    case 11:
        word[0]='e';
        word[1]='n';
        word[2]='d';
        word[3]='s';
        word[4]='i';
        word[5]='e';
        word[6]='g';
        break;
    case 12:
        word[0]='b';
        word[1]='a';
        word[2]='c';
        word[3]='k';
        word[4]='f';
        word[5]='i';
        word[6]='s';
        word[7]='c';
        word[8]='h';
        break;
    case 13:
        word[0]='p';
        word[1]='r';
        word[2]='o';
        word[3]='g';
        word[4]='r';
        word[5]='a';
        word[6]='m';
        word[7]='m';
        word[8]='i';
        word[9]='e';
        word[10]='r';
        word[11]='e';
        word[12]='r';
        break;
    case 14:
        word[0]='n';
        word[1]='o';
        word[2]='r';
        word[3]='d';
        word[4]='s';
        word[5]='e';
        word[6]='e';
        break;
    case 15:
        word[0]='a';
        word[1]='e';
        word[2]='r';
        word[3]='m';
        word[4]='e';
        word[5]='l';
        word[6]='k';
        word[7]='a';
        word[8]='n';
        word[9]='a';
        word[10]='l';
        break;
    case 16:
        word[0]='v';
        word[1]='o';
        word[2]='d';
        word[3]='k';
        word[4]='a';
        break;
    case 17:
        word[0]='k';
        word[1]='a';
        word[2]='p';
        word[3]='h';
        word[4]='o';
        word[5]='r';
        word[6]='n';
        break;
    case 18:
        word[0]='s';
        word[1]='u';
        word[2]='e';
        word[3]='z';
        word[4]='k';
        word[5]='a';
        word[6]='n';
        word[7]='a';
        word[8]='l';
        break;
    case 19:
        word[0]='t';
        word[1]='e';
        word[2]='c';
        word[3]='h';
        word[4]='n';
        word[5]='i';
        word[6]='k';
        break;
    case 20:
        word[0]='L';
        word[1]='i';
        word[2]='e';
        word[3]='c';
        word[4]='h';
        word[5]='t';
        word[6]='e';
        word[7]='n';
        word[8]='s';
        word[9]='t';
        word[10]='e';
        word[11]='i';
        word[12]='n';
        break;
    case 21:
        word[0]='k';
        word[1]='a';
        word[2]='r';
        word[3]='t';
        word[4]='o';
        word[5]='f';
        word[6]='f';
        word[7]='e';
        word[8]='l';
        word[9]='p';
        word[10]='u';
        word[11]='f';
        word[12]='f';
        word[13]='e';
        word[14]='r';
        break;
    case 22:
        word[0]='o';
        word[1]='p';
        word[2]='f';
        word[3]='e';
        word[4]='r';
        word[5]='a';
        word[6]='n';
        word[7]='o';
        word[8]='d';
        word[9]='e';
        break;
    case 23:
        word[0]='n';
        word[1]='e';
        word[2]='u';
        word[3]='0';
        word[4]='a';
        word[5]='m';
        word[6]='s';
        word[7]='t';
        word[8]='e';
        word[9]='r';
        word[10]='d';
        word[11]='a';
        word[12]='m';
        break;
    case 24:
        word[0]='z';
        word[1]='y';
        word[2]='k';
        word[3]='l';
        word[4]='o';
        word[5]='p';
        break;
    case 25:
        word[0]='g';
        word[1]='y';
        word[2]='n';
        word[3]='a';
        word[4]='e';
        word[5]='k';
        word[6]='o';
        word[7]='l';
        word[8]='o';
        word[9]='g';
        word[10]='e';
        break;
    case 26:
        word[0]='g';
        word[1]='a';
        word[2]='z';
        word[3]='a';
        word[4]='s';
        word[5]='t';
        word[6]='r';
        word[7]='e';
        word[8]='i';
        word[9]='f';
        word[10]='e';
        word[11]='n';
        break;
    case 27:
        word[0]='s';
        word[1]='c';
        word[2]='a';
        word[3]='r';
        word[4]='a';
        word[5]='b';
        word[6]='a';
        word[7]='e';
        word[8]='u';
        word[9]='s';
        break;
    case 28:
        word[0]='f';
        word[1]='a';
        word[2]='u';
        word[3]='p';
        word[4]='a';
        word[5]='x';
        break;
    case 29:
        word[0]='z';
        word[1]='y';
        word[2]='k';
        word[3]='l';
        word[4]='o';
        word[5]='p';
        break;
    case 30:
        word[0]='r';
        word[1]='u';
        word[2]='m';
        word[3]='e';
        word[4]='n';
        word[5]='e';
        word[6]='r';
        word[7]='c';
        word[8]='h';
        word[9]='o';
        word[10]='o';
        break;
    case 31:
        word[0]='b';
        word[1]='u';
        word[2]='s';
        word[3]='e';
        word[4]='f';
        word[5]='u';
        word[6]='c';
        word[7]='k';
        break;
    case 32:
        word[0]='w';
        word[1]='h';
        word[2]='e';
        word[3]='e';
        word[4]='l';
        word[5]='f';
        word[6]='a';
        word[7]='r';
        word[8]='m';
        word[9]='e';
        word[10]='r';
        break;
    case 33:
        word[0]='d';
        word[1]='i';
        word[2]='n';
        word[3]='u';
        word[4]='t';
        word[5]='u';
        break;
    case 34:
        word[0]='c';
        word[1]='o';
        word[2]='c';
        word[3]='0';
        word[4]='0';
        word[5]='g';
        word[6]='r';
        word[7]='u';
        word[8]='p';
        word[9]='p';
        word[10]='e';
        break;
    case 35:
        word[0]='u';
        word[1]='g';
        word[2]='a';
        word[3]='n';
        word[4]='d';
        word[5]='a';
        word[6]='n';
        word[7]='0';
        word[8]='k';
        word[9]='n';
        word[10]='u';
        word[11]='c';
        word[12]='k';
        word[13]='e';
        word[14]='l';
        break;
    case 36:
        word[0]='g';
        word[1]='i';
        word[2]='z';
        word[3]='u';
        word[4]='e';
        word[5]='l';
        break;
    case 37:
        word[0]='a';
        word[1]='n';
        word[2]='d';
        word[3]='i';
        word[4]='0';
        word[5]='d';
        word[6]='e';
        word[7]='r';
        word[8]='0';
        word[9]='n';
        word[10]='a';
        word[11]='z';
        word[12]='i';
        break;
    case 38:
        word[0]='h';
        word[1]='o';
        word[2]='b';
        word[3]='i';
        word[4]='n';
        word[5]='g';
        word[6]='e';
        word[7]='r';
        break;
    case 39:
        word[0]='a';
        word[1]='b';
        word[2]='d';
        word[3]='u';
        word[4]='e';
        word[5]='l';
        break;
    default:
    {
        cleanup();
        Logo();
        printf("Generation failed!\nTry it again!\n");
    };
    break;
    }

    cleanup();
    //fflush(stdin);
    Logo();
    printf("Finish! Let's begin!\nPress Enter!");
    dummy = getch();
    cleanup();
    int abgleich;
    if(round == 0)
    {
        for(abgleich = 0; abgleich < 15; abgleich++)
        {
            if(word[abgleich] == '0')
            {
                speicher[abgleich] = ' ';
            }
        }
    }
    int tryss = 0;
    while(trys < 10)
    {
        int vorhanden = 0;
        int unterstrich = 0;
        cleanup();
        switch(trys)
        {
        case 1:
            printf("\t^\n");
            break;
        case 2:
            printf("\t|\n");
            printf("\t|\n");
            printf("\t|\n");
            printf("\t|\n");
            printf("\t|\n");
            printf("\t|\n");
            printf("\t^\t\n");
            break;
        case 3:
            printf("\t|/\n");
            printf("\t|\n");
            printf("\t|\n");
            printf("\t|\n");
            printf("\t|\n");
            printf("\t|\n");
            printf("\t^\t\n");
            break;
        case 4:
            printf("\t__________\n");
            printf("\t|/\n");
            printf("\t|\n");
            printf("\t|\n");
            printf("\t|\n");
            printf("\t|\n");
            printf("\t|\n");
            printf("\t^\t\n");
            break;
        case 5:
            printf("\t__________\n");
            printf("\t|/\t |\n");
            printf("\t| \t |\n");
            printf("\t|\n");
            printf("\t|\n");
            printf("\t|\n");
            printf("\t|\n");
            printf("\t^\t\n");
            break;
        case 6:
            printf("\t__________\n");
            printf("\t|/\t |\n");
            printf("\t| \t |\n");
            printf("\t| \t O\n");
            printf("\t|\n");
            printf("\t|\n");
            printf("\t|\n");
            printf("\t^\t\n");
            break;
        case 7:
            printf("\t__________\n");
            printf("\t|/\t |\n");
            printf("\t| \t |\n");
            printf("\t| \t O\n");
            printf("\t| \t |\n");
            printf("\t| \t |\n");
            printf("\t|\n");
            printf("\t^\t\n");
            break;
        case 8:
            printf("\t__________\n");
            printf("\t|/\t |\n");
            printf("\t| \t |\n");
            printf("\t| \t O\n");
            printf("\t| \t\\|/\n");
            printf("\t| \t |\n");
            printf("\t|\n");
            printf("\t^\t\n");
            break;
        case 9:
            printf("\t__________\n");
            printf("\t|/\t |\n");
            printf("\t| \t |\n");
            printf("\t| \t O\n");
            printf("\t| \t\\|/\n");
            printf("\t| \t |\n");
            printf("\t| \t ^\n");
            printf("\t^\t\n");
            break;
        }
        printf("%s\n", speicher);
        printf("Gib einen Buchstaben ein! -->  ");
        eingabe = getchar();

        for(abgleich = 0; abgleich < 15; abgleich++)
        {
            if(word[abgleich] == eingabe)
            {
                speicher[abgleich] = eingabe;
                vorhanden++;
            }
        }
        int abgleich1;
        for(abgleich1 = 0; abgleich1 < 15; abgleich1++)
        {
            if(speicher[abgleich1] == '_')
            {
                unterstrich++;
            }
        }
        tryss = trys;
        if(vorhanden == 0)
        {
            trys++;
        }

        if(unterstrich == 0)
        {
            trys = 100;
        }


        round++;
    }
    cleanup();
    int i;
    int exit = 0;
    if(trys == 100)
    {
        Logo();
        printf("Das gesuchte Wort: %s\n", speicher);
        printf("Du hast das gesuchte Wort in %d Versuchen gefunden und\nhast %d falschen Buchstaben eingegeben!\n",round, tryss);
        system("pause");
    }
    else
    {
        Logo();
        printf("Du hast das gesuchte Wort nicht gefunden!\n0");
        printf("Das Wort war: ");
        for(i = 0; i < 16; i++)
        {
            if (word[i] != '0')
            {
                printf("%c", word[i]);
            }

        }
        printf("\n");
        printf("Du kehrst nun zum Lerum-Startbildschirm zurueck!");
        dummy = getch();
        cleanup();
    }
}

int Random(int randeinstellung, int schwkeit)
{
    int zahl1;
    const int rand1 = randeinstellung;

    srand((unsigned) time(NULL));
    zahl1 = rand()%rand1;

    switch(schwkeit)
    {
    case 2:
        zahl1 = zahl1 + 10;
        break;
    case 3:
        zahl1 = zahl1 + 20;
        break;
    case 4:
        zahl1 = zahl1 + 30;
        break;
    }
    return zahl1;
}

void Logo()
{
    printf("\t__________\n");
    printf("\t|/\t |\n");
    printf("\t| \t |\n");
    printf("\t| \t O\n");
    printf("\t| \t\\|/\n");
    printf("\t| \t |\n");
    printf("\t| \t ^\n");
    printf("\t^\n");

}
void Start()
{
    char dummy;

    cleanup();

    Logo();
    printf("\n\Hangman Version 0.3.1.0 Go\n");
    printf("\nAlle Woerter sind klein geschrieben,\nGross-Klein Schreibung wurde absichtlich ignoriert!\n%c = ae, %c = oe, %c = ue, %c = ss",132,148,129,225);
    dummy = getch();
    cleanup();
    Hangman();


}
