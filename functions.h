#ifndef FUNCTIONS_H_INCLUDED
#define FUNCTIONS_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <conio.h>
#include <strings.h>
#include <time.h>
#include "functions.c"
#include "globalvars.h"
#ifdef _WIN32
    #define CLEAR "cls"
#else //In any other OS
    #define CLEAR "clear"
#endif
//Copyright Niklas Haiden, Fabian Hofstaetter, Alexander Temmel
void cleanup();
void failure();
void help();
void version();
void beenden();
void failureeingabe();

#endif // FUNCTIONS_H_INCLUDED
