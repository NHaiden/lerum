#ifndef HANGMAN_H_INCLUDED
#define HANGMAN_H_INCLUDED

#include "functions.h"
#include "hangman.c"
//Copyright Niklas Haiden, Fabian Hofstaetter, Alexander Temmel
void Start();
void Hangman();
int Random();
void Logo();

#endif // HANGMAN_H_INCLUDED
