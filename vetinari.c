#include "vetinari.h"

//Copyright Niklas Haiden, Fabian Hofstaetter, Alexander Temmel
void vecopyright()
{
    char dummy;
    cleanup();
    printf("\n\tOriginale Code-Basis von Simon Wolfhardt\n\tNeue Features sowie Verbesserungen: Niklas Haiden\n");
    dummy = getch();
    cleanup();
}

void vemenue()
{
    char user;
    int x = 1;
    cleanup();
    char dummy;

    do
    {

        printf("\n\tVetinari 0.2.0.0 \"Sekai\"\n");
        printf("\n\t(1)Starte Vetinari\n");
        printf("\t(2)Zeige Copyright-Infos\n");
        printf("\t(3)Beende Vetinari\n");

        user = getch();


        switch(user)
        {
        case '1':
            vegame();
            break;
        case '2':
            vecopyright();
            break;
        case '3':
            cleanup();
            printf("\n\tDu kehrst nun zur%cck!", ue);
            dummy = getch();
            cleanup();
            x = 0;
            return;
            break;
        default:
            cleanup();
            failure();
            cleanup();
            break;
        }

    }
    while (x == 1);
}






void vegame()
{
    int x = 'X', dmg=0, hp=100, hp2=100, i, ii, dmg2;
    double buff = 1;
    char name[]= "Trainingspuppe";
    char dummy;

    cleanup();

    do
    {
        i = hp / 2;
        ii = 50 - i;

       printf("Leben                - ");

        for (i; i > 0; i--)
        {
            printf("|");
        }
        for (ii; ii > 0; ii--)
        {
            printf(".");
        }

        printf("  %d", hp);

        i = hp2 / 2;
        ii = 50 - i;

        printf("\nTrainingspuppe Leben - ");
        for (i; i > 0; i--)
        {
            printf("|");
        }
        for (ii; ii > 0; ii--)
        {
            printf(".");
        }

        printf("  %d", hp2);

        putchar('\n');
        printf("\n%s will k%cmpfen:\n(1) Schlag     (2) Tackle\n(3) Kopfnuss   (4) Beleidigen\n", name, ae);

        x = getch();

        /*Spieler ist dran*/
        dmg = 0;
        switch(x)
        {
        case '1':
            dmg = buff * 20;
            printf("\n%d HP wurden dem Gegner abgezogen!\n", dmg);
            break;
        case '2':
            dmg = 25;
            printf("\n%d HP wurden dem Gegner abgezogen!\n", dmg);
            break;
        case '3':
            dmg = 45 * buff;
            hp = hp - 20;
            printf("\n%d HP wurden dem Gegner abgezogen!\nDu hast dich selbst verletzt! -20 HP", dmg);
            break;
        case '4':
            buff = 1.3;
            printf("\nDein Gegner ist beleidigt! Du machst 30%% mehr Schaden\n", dmg);
            break;
        }

        putchar('\n');

        /*Gegner ist dran*/
        hp2 = hp2 - dmg;

        dmg2 = (rand() % 6) + 17;
        printf("\nDer Gegner hat dir %d Schaden gemacht!\n\n", dmg2);
        dummy = getch();
        hp = hp - dmg2;
        /*Gegner ist fertig*/
        cleanup();
    }
    while (hp > 0 && hp2 > 0);

    if (hp <= 0 && hp2 <= 0)
    {
        printf("Es ist ein Unentschieden!");
        dummy = getch();
        cleanup();
        return;
    }


    else if (hp <= 0)
    {
        hp = 0;
        printf("Du hast verloren...");
        dummy = getch();
        cleanup();
        return;
    }
    else if (hp2 <= 0)
    {
        hp2 = 0;
        cleanup();
        printf("\n\tDu hast gewonnen!\n");
        dummy = getch();
        cleanup();
        return;
    }

    i = hp / 2;
    ii = 50 - i;

    printf("Leben                - ");

    for (i; i > 0; i--)
    {
        printf("|");
    }
    for (ii; ii > 0; ii--)
    {
        printf(".");
    }

    printf("  %d", hp);

    i = hp2 / 2;
    ii = 50 - i;

    printf("\n%s Leben - ", name);
    for (i; i > 0; i--)
    {
        printf("|");
    }
    for (ii; ii > 0; ii--)
    {
        printf(".");
    }
    printf("  %0d", hp2);
}

/*
int randname()
{
 return 1;
}
*/
