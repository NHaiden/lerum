# Lerum
Lerum is a suite of open-source, selfmade & polished games. Just pick which you want. Currently only compatible with Windows, Linux to come.

# How does it work?
Lerum is a suite of apps, called Nerum, Hangman and Vetinari, which are different kinds of games. Every
game has it's unique name, version and features. On Lerum's Interface, called Naruy, you can view different
informations and you can select which game you want to play. Just hit the number on your keyboard, and off you go!

# Copyright
Copyright goes to Niklas, Fabian & Alexander.

