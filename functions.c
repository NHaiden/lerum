#include "functions.h"
#ifdef _WIN32
    #define CLEAR "cls"
    #define GFLAG 0
#else //In any other OS
    #define CLEAR "clear"
    #define GFLAG 1
#endif
//Copyright Niklas Haiden, Fabian Hofstaetter, Alexander Temmel
//Allgemeine Funktionen
char codename[] = "Vetinari";
char versionstring[]  = "1.1.2.0";
void cleanup()
{
    system(CLEAR);
}
    #ifdef GFLAG==1
        initscr();
        cbreak();
        noecho();
        nonl();
        intrflush(stdscr, FALSE);
        keypad(stdscr, TRUE);
        #endif // GFLAG



void beenden()
{
    printf("\n\n\tHoffentlich sehen wir uns bald wieder :)\n\n");
    char dummy;
    dummy = getch();
    #ifdef _WIN32
    exit(0);
    #else
    endwin();
    exit(EXIT_SUCCESS);
    #endif // _WIN32
}

void failure()
{
    char dummy;
    cleanup();
    printf("\n\tDiese Option steht nicht zur Auswahl.\n");
    dummy = getch();
}

void failureeingabe()
{
    char dummy;
    cleanup();
    printf("\n\tWolltest du etwas falsches eingeben?\n");
}


void version()
{
    printf("\tLerum %s \"%s\"", versionstring, codename);
}
void help()
{
    char user;
    char dummy;
    char changelogwahl;
    do
    {
        version();
        printf("\n\tHilfe & Infos");
        puts("");
        printf("\n\t(1)Lizenz\n");
        printf("\t(2)Copyright\n");
        printf("\t(3)Changelogs ansehen\n");
        printf("\t(4)What's coming\n");
        printf("\t(5)Exit to Menue");
        puts("");
        printf("\n\tAuswahl -> ");

        user = getche();
        cleanup();
        switch(user)
        {
        case '1':
            printf("\n\tLerum ist freie Software, stehend unter der GNU Public License V3.0.\n");
            printf("\n\tHiermit wird es dir erlaubt, Teile von Lerum zu verwenden, solange du deine Software ebenfalls unter Open-Source freigibst.\n");
            dummy = getch();
            cleanup();
            break;
        case '2':
            cleanup();
            printf("Copyright geht an Alexander Temmel, Niklas Haiden sowie Fabian Hofstaetter.\nWir haben alle dazu beigetragen, was Lerum heute ist. Danke!\n");
            dummy = getch();
            cleanup();
            break;

        case '3':
            printf("\n\tWelchen Changelog moechtest du dir anschauen?\n");
            printf("\n\t(1)Changelog von Nerum");
            printf("\n\t(2)Changelog von Hangman");
            printf("\n\t(3)Changelog von Vetinari");
            printf("\n\t(4)Changelog von Naruy\n");
            printf("\n\t-> ");
            changelogwahl = getch();
            switch(changelogwahl)
            {
            case '1':
                changelog(1);
                dummy = getch();
                cleanup();
                break;
            case '2':
                changelog(2);
                dummy = getch();
                cleanup();
                break;
            case '3':
                changelog(3);
                dummy = getch();
                cleanup();
                break;
            case '4':
                changelog(4);
                dummy = getch();
                cleanup();
                break;
            default:
                cleanup();
                failure();
            }
            break;
        case '4':
            cleanup();
            printf("\n\tBald kommt ein Highscore Feature namens Nury, sowie neue Woerter in Hangman und Charaktere in Vetinari.");
            printf("\n\tAlso haltest Ausschau nach Lerum 2.0!\n");
            dummy = getch();
            cleanup();
            break;

        case '5':
            printf("\t\nDu kehrst nun zurueck.\n");
            dummy = getch();
            cleanup();
            return;
            break;
        default:
            cleanup();
            failure();
            cleanup();
            break;

        }
    }
    while(user != '5');
    cleanup();
}

void changelog(int what)
{
    switch(what)
    {
    case 1:
        printf("\n\tKeine Eintragung.");
        break;
    case 2:
        printf("\n\tKeine Eintragung.");
        break;
    case 3:
        printf("\n\tInital Release.");
        break;
    case 4:
        printf("\n\tImprovments.");

        break;
    }
    return;

}

int randnumbers(int min, int max)
{
    srand((unsigned)time(NULL));

    int randwert;

    randwert = rand() % (max-min+1) + min;
    return randwert;
}
