#include "functions.h"
#include "nerum.h"
char neversion[] = "V0.7.2.0";
char necodename[] = "Groove";
void nemenu()
{
    char user;
    cleanup();
    do{
                printf("\n\tNerum %s \"%s\"", neversion, necodename);
                printf("\n\t(1) Fragesteller vs. Unwissenden\n");
                printf("\t(2) Player vs. Player\n");
                printf("\t(3) Gegen Bot\n");
                printf("\t(4) Zurueck\n");
            //    printf("\t(4) Wieviele Versuche brauchst du?\n");
                user = getch();
                switch(user)
                {
                case '1':
                    game1();
                    break;
                case '2':
                    DuoPlayer();
                    break;
                case '3':
                    gegenBot();
                    break;
                case '4':

                    cleanup();
                    return;
                    break;
                default:
                    failure();
                    cleanup();
                    break;
                }
}
while(user != '1' || user != '2' || user != '3' || user != '4');
}

void game1()
{
    cleanup();
    char fragesteller[4096];
    char unwissender[4096];
    int versuche;
    int zahl;
    char usereingabe[4096];
    char dummy;
    int ok;


    printf("\tWer ist der Fragesteller? --> ");
    gets(fragesteller);

    printf("\tWer soll die Zahl erraten? --> ");
    gets(unwissender);


    do
    {
        printf("\t%s, wieviele Versuche gibst du %s -> ", fragesteller, unwissender);
        ok = sscanf(gets(usereingabe), "%d%c", &versuche, &dummy);
        if(ok != 1)
        {
            cleanup();
            failureeingabe();
            dummy = getch();
            cleanup();
        }
    }
    while(ok != 1);


    do
    {
        printf("\t%s, welche Zahl soll %s erraten? -> ", fragesteller, unwissender);
        ok = sscanf(gets(usereingabe), "%d%c", &zahl, &dummy);
        if(ok != 1)
        {
            cleanup();
            failureeingabe();
            dummy = getch();
            cleanup();
        }
    }
    while(ok != 1);

    cleanup();
    printf("\n\t%s, du hast von %s %d Versuche bekommen.\n\tVerplempere sie nicht!\n", unwissender, fragesteller, versuche);
    dummy = getch();
    cleanup();

    int count = 0;
    int done = 0;
    int z = 0;
    int uantw;
    int i = 0; //Z�hlervariable
    char user;
    char safe; //F�r neue Eingabepr�fung
    int hastnoch = versuche;
    while (done < 1)
    {
        if (i == versuche)
        {
            printf("\n\t%s, du hast zuviele Versuche verbraten.\n", unwissender, fragesteller, versuche);
            done++;
            z++;
        }
        do
        {
            uantw = 0;
            printf("\n\tDu hast noch %d Versuche.", versuche-i);
            printf("\n\t%s, gib eine Zahl ein -> ", unwissender);
            ok = sscanf(gets(usereingabe), "%d%c", &uantw, &dummy);

            if(ok != 1)
            {
                cleanup();
                failureeingabe();
                dummy = getch();
                cleanup();
            }
        }
        while(ok != 1);
        count++;
        z = 0;
        while (z < 1)
        {
            if (uantw == zahl)
            {
                printf("\n\tDas ist korrekt, %s!\n", unwissender);
                printf("\tDu hast %d Versuche gebraucht.\n", count);
                printf("\tDu kehrst zum Hauptmen%c zur%cck.", ue, ue);
                dummy = getch();
                cleanup();
                return;


            }
            if (uantw < zahl)
            {
                cleanup();
                printf("\tRate nochmal! Deine Antwort ist zu klein, %s\n", unwissender);
                dummy = getch();
                cleanup();
                z++;
                i++;
            }
            if (uantw > zahl)
            {
                cleanup();
                printf("\tRate nochmal! Dein Antwort ist zu gross, %s\n", unwissender);
                dummy = getch();
                cleanup();
                z++;
                i++;
            }
        }
    }
}
int gegenBot()
{
    char dummy;
    char username[4096];
    char usereingabe[4096];
    char schwierigkeit;
    int zahl;
    int randwert;
    int versuche;
    int fertig = 0;
    srand((unsigned)time(NULL));
    cleanup();
    printf("\n\tWie ist dein Name? -> ");
    gets(username);
    cleanup();
    printf("\tHallo, %s, ich bin Verge.\n\tIch bin der Bot der dir eine Zahl geben wird, die du erraten musst!\n\tAlso, beginnen wir! Druecke ENTER um weiterzukommen!\n",username);
    dummy = getch();
    do
    {
        cleanup();
        printf("\t(1)Einfach - Eine Randomzahl, dreistellig und 10 Versuche\n");
        printf("\t(2)Mittel - Eine Randomzahl , vierstellig und 15 Versuche\n");
        printf("\t(3)Schwer - Eine Randomzahl , f%cnfstellig und 25 Versuche\n", ue);
        printf("\tWelche Schwierigkeit soll ich dir geben, %s?\n",username);
        printf("\n\tGib ein -> ");
        schwierigkeit = getch();
        if(schwierigkeit == '1' || schwierigkeit == '2' || schwierigkeit == '3')
        {
            fertig = 1;
            cleanup();
        }
        else
        {
            cleanup();
            failure();
            cleanup();
            fertig = 0;
        }

    }

    while(fertig != 1);
    cleanup();
    printf("\tNa dann kanns ja losgehen! Viel Spass %s!\n", username);
    printf("\tBei jeder Eingabe ist eine Pr%cfung vorhanden, sodass du nix falsches eingeben kannst!\n",ue);
    dummy = getch();
    cleanup();
    switch(schwierigkeit)
    {
    case '1':
       zahl = randnumbers(100,999);
       versuche = 10;
    break;
    case '2':
        zahl = randnumbers(1000,9999);
        versuche = 15;
        break;
    case '3':
        zahl = randnumbers(10000,99999);
        versuche = 25;
        break;

    }
    int count = 0;
    int done = 0;
    int z = 0;
    int uantw;
    int i = 0; //Z�hlervariable
    int ok; //F�r neue Eingabepr�fung;
    char user;
    char safe; //F�r neue Eingabepr�fung
    int uebrig = 0;
    while (done < 1)
    {
        if (i == versuche)
        {
            printf("\tDu hast zuviele Versuche verbraten. Du hattest %d Versuche.\n", versuche);
            printf("\tDie Zahl war %d!", zahl);
            dummy = getch();
            cleanup();
            return 0;
            done++;
            z++;
        }
        do
        {
            uebrig = versuche - i;
            printf("\n\tDu hast noch %d Versuche, %s\n", uebrig, username);
            printf("\t%s, was vermutest du? -> ", username);
            uantw = 0;
            ok = sscanf(gets(usereingabe), "%d%c", &uantw, &safe);
            if (ok != 1)
            {
                cleanup();
                failureeingabe();
                dummy = getch();
                cleanup();
            }
        }
        while(ok != 1);
        count++;
        z = 0;
        while (z < 1)
        {
            if (uantw == zahl)
            {
                printf("\tDie Zahl war korrekt, %s!\n", username);
                printf("\tDu hast %d Versuche von %d Versuchen gebraucht.\n", count, versuche);
                dummy = getch();
                cleanup();
                printf("\tDr%ccke irgendeine Taste um zur%cck zu kehren!", oe, ue);
                dummy = getch();
                cleanup();
                return 0;
            }
            if (uantw < zahl)
            {
                cleanup();
                printf("\t%s, rate nochmal!\n\tHinweis: Die Zahl ist zu klein!\n", username);
                dummy = getch();
                cleanup();
                z++;
                i++;
            }
            if (uantw > zahl)
            {
                cleanup();
                printf("\t%s, rate nochmal!\n\tHinweis: Die Zahl ist zu gross!\n", username);
                dummy = getch();
                cleanup();
                z++;
                i++;
            }
        }
    }
}

void DuoPlayer()
{

    char dummy;
    char player1[4096];
    char player2[4096];
    char usereingabe[4096];
    int zahlP1, zahlP2;
    int senfP1, senfP2;
    int round = 0;
    int richtigP1 = 0, richtigP2 = 0;
    int ok;
    char safe;
    cleanup();
    printf("\n\tWie heisst der erste Spieler? --> ");
    gets(player1);

    printf("\n\tWie heisst der zweite Spieler? --> ");
    gets(player2);
    cleanup();

    printf("\n\t%s welche Zahl soll %s erraten?\n\t-> ", player1, player2);
    do
    {
        ok = sscanf(gets(usereingabe),"%d%c", &zahlP1, &safe);
        if(ok != 1)
        {
            cleanup();
            failureeingabe();
            cleanup();
        }
    }
    while(ok != 1);

    printf("\n\t%s welche Zahl soll %s erraten?\n\t-> ", player2, player1);
    do
    {
        ok = sscanf(gets(usereingabe),"%d%c", &zahlP2, &safe);
        if(ok != 1)
        {
            cleanup();
            failureeingabe();
            cleanup();
        }
    }
    while(ok != 1);
    int finish = 0;
    do
    {
        round++;
        printf("\tRound: %d\n", round);

        do
        {
            printf("%s, gib deinen Senf ab --> ", player1);
            ok = sscanf(gets(usereingabe),"%d%c", &senfP1, &safe);
            if(ok != 1)
            {
                cleanup();
                failureeingabe();
                cleanup();
            }
        }
        while(ok != 1);
        if(senfP1 == zahlP1)
        {
            printf("Dein Senf schmeckt gut und ist perfekt dosiert %s, du hast %d versuche gebraucht!\n", player1, round);
            finish = 1;
            richtigP1 = 1;
        }
        else if (senfP1 > zahlP1)
        {
            printf("Ueber treib mal nicht mit Senf, %s!\n", player1);
        }
        else
        {
            printf("Das Brot ist groesser als ein millimeter, %s!\n", player1);
        }

        do
        {
            printf("%s, gib deinen Senf ab --> ", player2);
            ok = sscanf(gets(usereingabe),"%d%c", &senfP2, &safe);
            if(ok != 1)
            {
                cleanup();
                failureeingabe();
                cleanup();
            }
        }
        while(ok != 1);
        printf("Dein Senf schmeckt gut und ist perfekt dosiert %s, du hast %d versuche gebraucht!\n", player2, round);
        if(senfP2 == zahlP2)
        {
            finish = 1;
            richtigP2 = 1;
        }
        else if (senfP2 > zahlP2)
        {
            printf("Nicht uebertreiben!, %s!\n", player2);
        }
        else
        {
            printf("Nicht untertreiben!, %s!\n", player2);
        }
        dummy = getch();
        cleanup();

        if(finish !=1)
        {
            printf("NEXT ROUND");
            dummy = getch();
            cleanup();
        }
    }
    while(finish != 1);
    if (richtigP1 > richtigP2)
    {
        printf("%s, dein Senf schmeckt besser als der von %s!", player1, player2);
    }
    else if(richtigP1 < richtigP2)
    {
        printf("%s, dein Senf schmeckt besser als der von %s!", player2, player1);
    }
    else
    {
        printf("Eine Mischung von Beidem ist top!");
    }
    putchar('\n');
    dummy = getch();
}

